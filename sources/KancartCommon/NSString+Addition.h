//
//  NSStringAddition.h
//  MCommerce
//
//  Created by apple on 11-3-21.
//  Copyright 2011 HonestWalker. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Addition)

/**
 * 验证字符串长度
 * @param str   待验证字符串
 * @param min   字符串长度最小值
 * @param max   字符串长度最大值
 * @return BOOL 是否满足长度要求,满足返回true/YES，不满足返回false/No
 */
+ (BOOL)lengthValidate:(NSString *)str
                   min:(NSUInteger)min
                   max:(NSUInteger)max;
/**
 * 验证字符串是否符合正则表达式
 * @param str 待验证字符串
 * @param regex 正则表达式字符串
 */
+ (BOOL)regexValidate:(NSString *)str withRegex:(NSString *)regex;

/**
 * 验证字符串是否是正确的email格式
 * @param str   待验证字符串
 * @return BOOL 是否是正确的email格式,格式正确返回true/YES，格式错误返回false/No
 */
+ (BOOL)IsEmail:(NSString *)str;

+ (NSUInteger)NumOfMatches:(NSString *)str regexString:(NSString *)regexString;

/**
 * 验证字符串是否为空
 * @param str   待验证字符串
 * @return BOOL 字符串是否为空,为空或者str不是字符串类型返回true/YES，不为空返回false/No
 */
+ (BOOL)IsNilOrEmpty:(NSString *)str;

/**
 * 验证字符串是否为电话号码
 * @param str   待验证字符串
 * @return BOOL 是否是正确的电话号码,格式正确返回true/YES，格式错误返回false/No
 */
+ (BOOL)IsPhone:(NSString *)str;

/**
 * 对字符串进行url编码
 * @return NSString 编码后的字符串
 */
- (NSString*)stringByURLEncodingStringParameter;

/**
 * 解析NVP格式的字符串(Name Value Pair / 名值对)
 * @param separator     间隔符(=)
 * @param delimiter     定界符(&)
 * @return NSDictionary 解析后的字典(Key-Value)
 */
- (NSDictionary *)parametersWithSeparator:(NSString *)separator
                                delimiter:(NSString *)delimiter;

+ (NSString *)htmlEncode:(NSString *)str;
+ (NSString *)htmlDecode:(NSString *)str;
+ (NSString *)replaceHTMLTag:(NSString *)str;

- (NSString*)stringByRemovingHTMLTags;

- (NSString *)stringByReplacingSnakeCaseWithCamelCase;
- (NSString *)stringByReplacingCamelCaseWithSnakeCase;

@end
