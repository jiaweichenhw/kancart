//
//  NSStringAddition.m
//  MCommerce
//
//  Created by apple on 11-3-21.
//  Copyright 2011 HonestWalker. All rights reserved.
//

#import "NSString+Addition.h"

@implementation NSString (Addition) 

/**
 * 验证字符串是否为空
 * @param str   待验证字符串
 * @return BOOL 字符串是否为空,为空或者str不是字符串类型返回true/YES，不为空返回false/No
 */
+ (BOOL)IsNilOrEmpty:(NSString *)str {
    if (![str isKindOfClass:[NSString class]]) {
        return YES;
    }
    
	if (str == nil) {
		return YES;
    }
	
	NSMutableString *string = [[NSMutableString alloc] init];
	[string setString:str];
	CFStringTrimWhitespace((__bridge CFMutableStringRef)string);
	if([string length] == 0)
	{
		return YES;
	}
	return NO;
}

/**
 * 验证字符串是否是正确的email格式
 * @param str   待验证字符串
 * @return BOOL 是否是正确的email格式,格式正确返回true/YES，格式错误返回false/No
 */
+ (BOOL)IsEmail:(NSString *)str {
    NSString *strRegex = @"^[a-zA-Z0-9_\\.]+@[a-zA-Z0-9-]+[\\.a-zA-Z]+$";
    return [NSString NumOfMatches:str regexString:strRegex];
}

+ (NSUInteger)NumOfMatches:(NSString *)str 
               regexString:(NSString *)regexString {
    NSRegularExpression *regexExp = [NSRegularExpression regularExpressionWithPattern:regexString options:NSRegularExpressionCaseInsensitive error:nil];
    NSUInteger numberOfMatches = [regexExp numberOfMatchesInString:str options:0 range:NSMakeRange(0, [str length])];
    //[@"" stringByReplacingOccurrencesOfString:@"" withString:@""];
    return numberOfMatches;
    
}

/**
 * 验证字符串长度
 * @param str   待验证字符串
 * @param min   字符串长度最小值
 * @param max   字符串长度最大值
 * @return BOOL 是否满足长度要求,满足返回true/YES，不满足返回false/No
 */
+ (BOOL)lengthValidate:(NSString *)string
                   min:(NSUInteger)min
                   max:(NSUInteger)max {
    if (string) {
        string = [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        if ([string length] >= min && [string length] <= max) {
            return YES;
        }
    }
    return NO;
}

/**
 * 验证字符串是否符合正则表达式
 * @param str 待验证字符串
 * @param regex 正则表达式字符串
 * @return BOOL
 */
+ (BOOL)regexValidate:(NSString *)str withRegex:(NSString *)regex {
    if ([NSString IsNilOrEmpty:regex]) {
        return YES;
    }
    
    NSRegularExpression *regexExppression = [NSRegularExpression regularExpressionWithPattern:regex options:NSRegularExpressionCaseInsensitive error:nil];
    NSUInteger numberOfMatches = [regexExppression numberOfMatchesInString:str options:0 range:NSMakeRange(0, [str length])];
    return numberOfMatches;
}


/**
 * 验证字符串是否为电话号码
 * @param str   待验证字符串
 * @return BOOL 是否是正确的电话号码,格式正确返回true/YES，格式错误返回false/No
 */
+ (BOOL)IsPhone:(NSString *)str {
    //电话号码正则表达式（支持手机号码，3-4位区号，7-8位直播号码，1－4位分机号）
    //NSString *strPhoneRegex = @"((\\d{11})|^((\\d{7,8})|(\\d{4}|\\d{3})-(\\d{7,8})|(\\d{4}|\\d{3})-(\\d{7,8})-(\\d{4}|\\d{3}|\\d{2}|\\d{1})|(\\d{7,8})-(\\d{4}|\\d{3}|\\d{2}|\\d{1}))$)";
    //电话号码正则表达式 ()
    NSString *strPhoneRegex = @"^[0-9+\\s\\-\\,\\(\\)]{0,45}$";
    NSRegularExpression *regexPhone = [NSRegularExpression regularExpressionWithPattern:strPhoneRegex options:NSRegularExpressionCaseInsensitive error:nil];
    NSUInteger numberOfMatches = [regexPhone numberOfMatchesInString:str options:0 range:NSMakeRange(0, [str length])];
    return numberOfMatches;
}

/**
 * 对字符串进行url编码
 * @return NSString 编码后的字符串
 */
- (NSString *)stringByURLEncodingStringParameter {
    // NSURL's stringByAddingPercentEscapesUsingEncoding: does not escape
    // some characters that should be escaped in URL parameters, like / and ?; 
    // we'll use CFURL to force the encoding of those
    //
    // We'll explicitly leave spaces unescaped now, and replace them with +'s
    //
    // Reference: [url]http://www.ietf.org/rfc/rfc3986.txt[/url]
	
    NSString *resultStr = self;
	
    CFStringRef originalString = (__bridge CFStringRef) self;
    CFStringRef leaveUnescaped = CFSTR(" ");
    CFStringRef forceEscaped = CFSTR("!*'();:@&=+$,/?%#[]");
	
    CFStringRef escapedStr;
    escapedStr = CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,
                                                         originalString,
                                                         leaveUnescaped, 
                                                         forceEscaped,
                                                         kCFStringEncodingUTF8);
	
    if( escapedStr ) {
        NSMutableString *mutableStr = [NSMutableString stringWithString:(__bridge NSString *)escapedStr];
        CFRelease(escapedStr);
		
        // replace spaces with plusses
        [mutableStr replaceOccurrencesOfString:@" "
                                    withString:@"%20"
                                       options:0
                                         range:NSMakeRange(0, [mutableStr length])];
        resultStr = mutableStr;
    }
    return resultStr;
}

/**
 * 解析NVP格式的字符串(Name Value Pair / 名值对)
 * @param separator     间隔符(=)
 * @param delimiter     定界符(&)
 * @return NSDictionary 解析后的字典(Key-Value)
 */
- (NSDictionary *)parametersWithSeparator:(NSString *)separator
                                delimiter:(NSString *)delimiter { 
    NSArray *parameterPairs = [self componentsSeparatedByString:delimiter]; 
    NSMutableDictionary *parameters = [NSMutableDictionary dictionaryWithCapacity:[parameterPairs count]]; 
    for (NSString *currentPair in parameterPairs) { 
        NSRange range = [currentPair rangeOfString:separator]; 
        if(range.location == NSNotFound) 
            continue; 
        NSString *key = [currentPair substringToIndex:range.location]; 
        NSString *value =[currentPair substringFromIndex:range.location + 1]; 
        [parameters setObject:[value stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding] forKey:key];
    } 
    return parameters; 
}

+ (NSString *)htmlEncode:(NSString *)str {
    str = [str stringByReplacingOccurrencesOfString:@"&" withString:@"&amp;"];
    str = [str stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
    str = [str stringByReplacingOccurrencesOfString:@"\"" withString:@"&quot;"];
    str = [str stringByReplacingOccurrencesOfString:@" " withString:@"&nbsp;"];
    str = [str stringByReplacingOccurrencesOfString:@"<" withString:@"&lt;"];
    str = [str stringByReplacingOccurrencesOfString:@">" withString:@"&gt;"];
    str = [str stringByReplacingOccurrencesOfString:@"\n" withString:@"<br />"];
    return str;
}

+ (NSString *)htmlDecode:(NSString *)str {
    str = [str stringByReplacingOccurrencesOfString:@"<br />" withString:@"\n"];
    str = [str stringByReplacingOccurrencesOfString:@"<br>" withString:@"\n"];
    str = [str stringByReplacingOccurrencesOfString:@"<br/>" withString:@"\n"];
    str = [str stringByReplacingOccurrencesOfString:@"<BR />" withString:@"\n"];
    str = [str stringByReplacingOccurrencesOfString:@"<BR>" withString:@"\n"];
    str = [str stringByReplacingOccurrencesOfString:@"<BR/>" withString:@"\n"];
    
    str = [str stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
    str = [str stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
    str = [str stringByReplacingOccurrencesOfString:@"&quot;" withString:@"\""];
    str = [str stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@" "];
    str = [str stringByReplacingOccurrencesOfString:@"&lt;" withString:@"<"];
    str = [str stringByReplacingOccurrencesOfString:@"&gt;" withString:@">"];
    str = [str stringByReplacingOccurrencesOfString:@"&ldquo;" withString:@"\""];
    str = [str stringByReplacingOccurrencesOfString:@"&rdquo;" withString:@"\""];
    str = [str stringByReplacingOccurrencesOfString:@"&lsquo;" withString:@"'"];
    str = [str stringByReplacingOccurrencesOfString:@"&rsquo;" withString:@"'"];
    return str;
}

+ (NSString *)replaceHTMLTag:(NSString *)str {
    NSError* error = NULL;
    NSRegularExpression* regex = [NSRegularExpression regularExpressionWithPattern:@"<[^>]+>"
                                                                           options:0
                                                                             error:&error];
    return [regex stringByReplacingMatchesInString:str
                                           options:0
                                             range:NSMakeRange(0, str.length)
                                      withTemplate:@""];
}

- (NSString*)stringByRemovingHTMLTags {
//    MarkupStripper* stripper = [[[MarkupStripper alloc] init] autorelease];
//    return [stripper parse:self];
    return nil;
}

- (NSString *)stringByReplacingSnakeCaseWithCamelCase {
    NSArray *components = [self componentsSeparatedByString:@"_"];
    NSMutableString *camelCaseString = [NSMutableString string];
    [components enumerateObjectsUsingBlock:^(NSString *component, NSUInteger idx, BOOL *stop) {
        if (idx > 0) {
            [camelCaseString appendString:[component capitalizedString]];
        } else {
            [camelCaseString appendString:component];
        }
    }];
    return [camelCaseString copy];
}

- (NSString *)stringByReplacingCamelCaseWithSnakeCase {
    NSUInteger index = 1;
    NSMutableString *snakeCaseString = [NSMutableString stringWithString:self];
    NSUInteger length = snakeCaseString.length;
    NSCharacterSet *characterSet = [NSCharacterSet uppercaseLetterCharacterSet];
    while (index < length) {
        if ([characterSet characterIsMember:[snakeCaseString characterAtIndex:index]]) {
            [snakeCaseString insertString:@"_" atIndex:index];
            index++;
        }
        index++;
    }
    return [snakeCaseString copy];
}

@end
