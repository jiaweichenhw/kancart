//
//  UIView+KCI18N.m
//  KanCart
//
//  Created by honestwalker on 1/15/14.
//  Copyright (c) 2014 honestwalker.com. All rights reserved.
//

#import "UIView+KCI18N.h"
#import <objc/runtime.h>
#import "KCI18NManager.h"
#import "UIView+KCI18N.h"
#import "NSString+KCI18N.h"

@implementation NSObject (KCI18N)

static char i18nKeys;

- (void)setI18NKeys:(NSMutableDictionary *)I18NKeys {
    // set
    objc_setAssociatedObject(self, &i18nKeys, I18NKeys, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSMutableDictionary *)I18NKeys {
    // get
    NSMutableDictionary *associatedObject = objc_getAssociatedObject(self, &i18nKeys);
    if (!associatedObject) {
        associatedObject = [[NSMutableDictionary alloc] initWithCapacity:4];
        [self setI18NKeys:associatedObject];
    }
    return associatedObject;
}

- (void)updateLanguage {
    NSArray *keys = self.I18NKeys.allKeys;
    for (NSString *stateStr in keys) {
        NSString *I18NName = [self.I18NKeys valueForKey:stateStr];
        [[KCI18NManager sharedInstance] removeObserver:self];
        
        if ([self isKindOfClass:UILabel.class]) {
            [(UILabel *)self setText:I18N(I18NName)];
        }
        if ([self isKindOfClass:UIButton.class] || [self.class isSubclassOfClass:UIButton.class]) {
            [(UIButton *)self setTitle:I18N(I18NName)
                              forState:[stateStr integerValue]];
        }
        if ([self isKindOfClass:UIViewController.class] || [self.class isSubclassOfClass:UIViewController.class]) {
            [(UIViewController *)self setTitle:I18N(I18NName)];
        }
        if ([self isKindOfClass:UIBarButtonItem.class]) {
            [(UIBarButtonItem *)self setTitle:I18N(I18NName)];
        }
        if ([self isKindOfClass:UITabBarItem.class]) {
            [(UITabBarItem *)self setTitle:I18N(I18NName)];
        }
        if ([self isKindOfClass:UITextField.class]) {
            [(UITextField *)self setPlaceholder:I18N(I18NName)];
        }
        if ([self isKindOfClass:UISearchBar.class]) {
            [(UISearchBar *)self setPlaceholder:I18N(I18NName)];
        }
    }
}

@end

@implementation UILabel (KCI18N)

- (void)kcI18NSetText:(NSString *)string {
    if ([NSStringFromClass(self.class) isEqualToString:@"UILabel"]) {
        [[KCI18NManager sharedInstance] replaceOld:self.text withNew:string observer:self];
        [self.I18NKeys setValue:string.KCI18NName
                         forKey:[NSString stringWithFormat:@"%d", 0]];
    }
//    NSLog(@"%@", NSStringFromClass(self.class));
    [self kcI18NSetText:string];
}

@end

@implementation UIButton (KCI18N)

- (void)kcI18NSetTitle:(NSString *)title forState:(UIControlState)state {
    [[KCI18NManager sharedInstance] replaceOld:[self titleForState:state] withNew:title observer:self];
    [self.I18NKeys setValue:title.KCI18NName
                     forKey:[NSString stringWithFormat:@"%lu", state]];
    [self kcI18NSetTitle:title forState:state];
}

@end


@implementation UIBarButtonItem (KCI18N)

- (id)kcI18NInitWithBarButtonSystemItem:(UIBarButtonSystemItem)systemItem
                                 target:(id)target
                                 action:(SEL)action {
    [self kcI18NInitWithBarButtonSystemItem:systemItem
                                     target:target
                                     action:action];
    switch (systemItem) {
        case UIBarButtonSystemItemDone:
            self.title = I18N(@"DONE");
            break;
        case UIBarButtonSystemItemEdit:
            self.title = I18N(@"EDIT");
            break;
        case UIBarButtonSystemItemCancel:
            self.title = I18N(@"CANCEL");
            break;
        default:
            break;
    }
    return self;
}

- (void)kcI18NSetTitle:(NSString *)title {
    [[KCI18NManager sharedInstance] replaceOld:self.title withNew:title observer:self];
    [self.I18NKeys setValue:title.KCI18NName
                     forKey:[NSString stringWithFormat:@"%d", 0]];
    [self kcI18NSetTitle:title];
}

- (id)kcI18NInitWithTitle:(NSString *)title
                    style:(UIBarButtonItemStyle)style
                   target:(id)target
                   action:(SEL)action {
    [self kcI18NInitWithTitle:title
                        style:style
                       target:target
                       action:action];
    if (self) {
        [[KCI18NManager sharedInstance] replaceOld:self.title withNew:title observer:self];
        [self.I18NKeys setValue:title.KCI18NName
                         forKey:[NSString stringWithFormat:@"%d", 0]];
    }
    return self;
}

- (void)kcI18NDealloc {
    [[KCI18NManager sharedInstance] removeObserver:self];
    [self kcI18NDealloc];
}

@end

@implementation UITabBarItem (KCI18N)

- (void)kcI18NSetTitle:(NSString *)title {
    [[KCI18NManager sharedInstance] replaceOld:self.title withNew:title observer:self];
    [self.I18NKeys setValue:title.KCI18NName
                     forKey:[NSString stringWithFormat:@"%d", 0]];
    [self kcI18NSetTitle:title];
}

- (id)kcI18NInitWithTitle:(NSString *)title
                    image:(UIImage *)image
                      tag:(NSInteger)tag {
    [self kcI18NInitWithTitle:title
                        image:image
                          tag:tag];
    if (self) {
        [[KCI18NManager sharedInstance] replaceOld:self.title withNew:title observer:self];
        [self.I18NKeys setValue:title.KCI18NName
                         forKey:[NSString stringWithFormat:@"%d", 0]];
    }
    return self;
}

- (void)kcI18NDealloc {
    [[KCI18NManager sharedInstance] removeObserver:self];
    [self kcI18NDealloc];
}

@end


@implementation UIViewController (KCI18N)

- (void)kcI18NSetTitle:(NSString *)title {
    [[KCI18NManager sharedInstance] replaceOld:self.title withNew:title observer:self];
    [self.I18NKeys setValue:title.KCI18NName
                     forKey:[NSString stringWithFormat:@"%d", 0]];
    self.tabBarItem.title = title;
    [self kcI18NSetTitle:title];
}

- (void)kcI18NDealloc {
    [[KCI18NManager sharedInstance] removeObserver:self];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self kcI18NDealloc];
}

@end

@implementation UITextField (KCI18N)

- (void)kcI18NSetPlaceholder:(NSString *)placeholder {
    [[KCI18NManager sharedInstance] replaceOld:self.placeholder withNew:placeholder observer:self];
    [self.I18NKeys setValue:placeholder.KCI18NName
                     forKey:[NSString stringWithFormat:@"%d", 0]];
    [self kcI18NSetPlaceholder:placeholder];
}

@end

@implementation UISearchBar (KCI18N)

- (void)kcI18NSetPlaceholder:(NSString *)placeholder {
    [[KCI18NManager sharedInstance] replaceOld:self.placeholder withNew:placeholder observer:self];
    [self.I18NKeys setValue:placeholder.KCI18NName
                     forKey:[NSString stringWithFormat:@"%d", 0]];
    [self kcI18NSetPlaceholder:placeholder];
}

@end
