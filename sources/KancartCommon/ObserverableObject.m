//
//  ObserverableObject.m
//  Kancart
//
//  Created by apple on 11-5-23.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import "ObserverableObject.h"

@implementation NSObject (ObserverableObject)

/** 为某个对象添加观察者
 * @param actionName
 * @param object
 * @return 无返回值
 */
- (void)addNotificationObserver:(NSString *)actionName
                         object:(id)object {
    SEL succeedSEL = NSSelectorFromString(@"actionSucceeded:");
    SEL failedSEL = NSSelectorFromString(@"actionFailed:");
    SEL errorSEL = NSSelectorFromString(@"actionError:");
    if (self && [self respondsToSelector:succeedSEL]) {
		[[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:succeedSEL
                                                     name:[NSString stringWithFormat:@"%@_succeeded", actionName]
                                                   object:object];
	}
    
    if (self && [self respondsToSelector:failedSEL]) {
		[[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:failedSEL
                                                     name:[NSString stringWithFormat:@"%@_failed", actionName]
                                                   object:object];
	}
    
    if (self && [self respondsToSelector:errorSEL]) {
		[[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:errorSEL
                                                     name:[NSString stringWithFormat:@"%@_error", actionName]
                                                   object:object];
	}
}

/** 为某个对象移除观察者
 * @param actionName
 * @param object
 * @return 无返回值
 */
- (void)removeNoticiationObserver:(NSString *)actionName
                           object:(id)object {
    [[NSNotificationCenter defaultCenter] removeObserver:self 
                                                    name:[NSString stringWithFormat:@"%@_succeeded", actionName] 
                                                  object:object];
    [[NSNotificationCenter defaultCenter] removeObserver:self 
                                                    name:[NSString stringWithFormat:@"%@_failed", actionName]
                                                  object:object];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:[NSString stringWithFormat:@"%@_error", actionName]
                                                  object:object];
}

@end
