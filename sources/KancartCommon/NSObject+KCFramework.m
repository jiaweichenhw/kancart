//
//  NSObject+KCFramework.m
//  KanCart
//
//  Created by honestwalker on 1/21/14.
//  Copyright (c) 2014 honestwalker.com. All rights reserved.
//

#import "NSObject+KCFramework.h"
#import <objc/runtime.h>

@implementation NSObject (KCFramework)

static char kcUserInfoKey;

- (void)setKcUserInfo:(NSMutableDictionary *)kcUserInfo {
    // set
    objc_setAssociatedObject(self, &kcUserInfoKey, kcUserInfo, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSMutableDictionary *)kcUserInfo {
    // get
    NSMutableDictionary *associatedObject = objc_getAssociatedObject(self, &kcUserInfoKey);
    if (!associatedObject) {
        associatedObject = [[NSMutableDictionary alloc] initWithCapacity:4];
        [self setKcUserInfo:associatedObject];
    }
    return associatedObject;
}

- (NSString *)shortDescription {
    NSString *desc = [NSString stringWithFormat:@"%@ 0x%02lx", NSStringFromClass(self.class), (unsigned long)self.hash];
    return desc;
}

@end
