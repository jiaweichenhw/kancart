//
//  UINavigationController+BackNotify.h
//  KanCart
//
//  Created by honestwalker on 1/16/14.
//
//

#import <UIKit/UIKit.h>

@interface UINavigationController (BackNotify)

+ (void)prepare;

@end
