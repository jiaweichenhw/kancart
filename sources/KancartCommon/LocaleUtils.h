//
//  LocaleUtils.h
//  Kancart
//
//  Created by honestwalker on 5/20/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface LocaleUtils : NSObject {
}
+ (NSString *)getCountryCode;
+ (NSString *)getLanguageCode;
@end

