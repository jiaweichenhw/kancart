//
//  NSURLRequest+Params.m
//  KancartAPI
//
//  Created by Jiawei Chen on 14/10/23.
//  Copyright (c) 2014年 honestwalker.com. All rights reserved.
//

#import "NSURLRequest+Params.h"
#import "NSString+Addition.h"

@implementation NSURLRequest (Params)

- (NSDictionary *)urlParameters {
    return [self.URL.query parametersWithSeparator:@"=" delimiter:@"&"];
}

- (BOOL)isAction:(NSString *)action {
    if ([self.URL.absoluteString rangeOfString:action].location != NSNotFound) {
        return YES;
    }
    return NO;
}

@end
