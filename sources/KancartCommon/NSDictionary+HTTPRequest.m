//
//  NSDictionary+HTTPRequest.m
//  KanCart
//
//  Created by Honestwalker MacVM on 7/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "NSDictionary+HTTPRequest.h"
#import "NSString+Addition.h"

@implementation NSDictionary (HTTPRequest)

- (NSString *)getPostData {
    NSMutableString *postData = [[NSMutableString alloc] init];
	BOOL hasPara = NO;
	NSEnumerator *enumerator = [self keyEnumerator];
	id key;
	while ((key = [enumerator nextObject])) {
		NSString *name = [key description];
		NSString *value = [[self objectForKey:key] description];
		// 忽略参数名或参数值为空的参数
		if(![NSString IsNilOrEmpty:name])
		{
			if (hasPara) {
				[postData appendString:@"&"];
			}
			[postData appendString:name];
			[postData appendString:@"="];
			[postData appendString:[value stringByURLEncodingStringParameter]];
			hasPara = YES;
		}
	}
	return [postData description];
}

@end
