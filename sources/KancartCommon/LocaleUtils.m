//
//  LocaleUtils.m
//  Kancart
//
//  Created by honestwalker on 5/20/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "LocaleUtils.h"

@implementation LocaleUtils

+ (NSString *)getCountryCode {
    NSLocale *currentLocale = [NSLocale currentLocale];
    return [currentLocale objectForKey:NSLocaleCountryCode];
}

+ (NSString *)getLanguageCode {
    NSLocale *currentLocale = [NSLocale currentLocale];
    return [currentLocale objectForKey:NSLocaleLanguageCode];
}

@end