//
//  MarkupStripper.m
//  KanCart
//
//  Created by honestwalker on 2/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MarkupStripper.h"

#import "EntityTables.h"

@implementation MarkupStripper

#pragma mark - NSXMLParserDelegate

- (void)parser:(NSXMLParser*)parser foundCharacters:(NSString*)string {
    [_strings addObject:string];
}

- (NSData*)             parser: (NSXMLParser*)parser
     resolveExternalEntityName: (NSString*)entityName
                      systemID: (NSString*)systemID {
    return [[[EntityTables sharedInstance] iso88591] objectForKey:entityName];
}

#pragma mark - Public
- (NSString*)parse:(NSString*)text {
    _strings = [[NSMutableArray alloc] init];
    
    NSString*     document  = [NSString stringWithFormat:@"<x>%@</x>", text];
    NSData*       data      = [document dataUsingEncoding:text.fastestEncoding];
    NSXMLParser*  parser    = [[NSXMLParser alloc] initWithData:data];
    parser.delegate = self;
    parser = nil;
    NSString* result = [_strings componentsJoinedByString:@""];
    _strings = nil;
    return result;
}


@end
