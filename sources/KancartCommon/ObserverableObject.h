//
//  ObserverableObject.h
//  Kancart
//
//  Created by apple on 11-5-23.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSObject (ObserverableObject)

/** 为某个对象添加观察者
 * @param actionName
 * @param object
 * @return 无返回值
 */
- (void)addNotificationObserver:(NSString *)actionName
                         object:(id)object;

/** 为某个对象移除观察者
 * @param actionName
 * @param object
 * @return 无返回值
 */
- (void)removeNoticiationObserver:(NSString *)actionName 
                           object:(id)object;
@end
