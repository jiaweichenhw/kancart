//
//  NSURLRequest+Params.h
//  KancartAPI
//
//  Created by Jiawei Chen on 14/10/23.
//  Copyright (c) 2014年 honestwalker.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSURLRequest (Params)

- (NSDictionary *)urlParameters;
- (BOOL)isAction:(NSString *)action;

@end
