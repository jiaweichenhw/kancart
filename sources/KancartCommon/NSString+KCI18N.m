//
//  NSString+KCI18N.m
//  KanCart
//
//  Created by honestwalker on 1/15/14.
//  Copyright (c) 2014 honestwalker.com. All rights reserved.
//

#import "NSString+KCI18N.h"
#import <objc/runtime.h>

@implementation NSString (KCI18N)

static char KCI18NNameKey;

- (void)setKCI18NName:(NSString *)KCI18NName {
    // set
    objc_setAssociatedObject(self, &KCI18NNameKey, KCI18NName, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSString *)KCI18NName {
    // get
    NSString *associatedObject = objc_getAssociatedObject(self, &KCI18NNameKey);
    return associatedObject;
}

- (id)kcI18NCopy {
    NSString *copy = [self kcI18NCopy];
    copy.KCI18NName = self.KCI18NName;
    return copy;
}

- (void)kcI18NDealloc {
    self.KCI18NName = nil;
    [self kcI18NDealloc];
}

@end
