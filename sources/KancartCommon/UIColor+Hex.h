//
//  UIColor+Hex.h
//  KancartAPI
//
//  Created by Jiawei Chen on 14/11/27.
//  Copyright (c) 2014年 honestwalker.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Hex)

+ (UIColor *)colorWithHex:(int)hex;

+ (UIColor *)colorWithHex:(int)hex withAlpha:(CGFloat)alpha;

+ (UIColor *)colorWithHexString:(NSString *)hexString;

+ (UIColor *)colorWithHexString:(NSString *)hexString withAlpha:(CGFloat)alpha;

@end
