//
//  KCRuntimeSwizzle.m
//  KanCart
//
//  Created by honestwalker on 1/7/14.
//  Copyright (c) 2014 honestwalker.com. All rights reserved.
//

#import <objc/runtime.h>
#import "KCRuntimeSwizzle.h"

@implementation KCRuntimeSwizzle

+ (void)swizzle:(Class)c
        origSel:(SEL)origSel
         newSel:(SEL)newSel {
    Method origMethod = class_getInstanceMethod(c, origSel);
    Method newMethod = class_getInstanceMethod(c, newSel);
    if (class_addMethod(c, origSel, method_getImplementation(newMethod), method_getTypeEncoding(newMethod))) {
        class_replaceMethod(c, newSel, method_getImplementation(origMethod), method_getTypeEncoding(origMethod));
    } else {
        method_exchangeImplementations(origMethod, newMethod);
    }
}

@end
