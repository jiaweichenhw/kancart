//
//  UIView+KCI18N.h
//  KanCart
//
//  Created by honestwalker on 1/15/14.
//  Copyright (c) 2014 honestwalker.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NSObject (KCI18N)

- (void)updateLanguage;

@end

@interface UILabel (KCI18N)

@end

@interface UIButton (KCI18N)

@end

@interface UINavigationBar (KCI18N)

@end

@interface UITabBarItem (KCI18N)

@end

@interface UIBarButtonItem (KCI18N)

@end

@interface UIViewController (KCI18N)

- (void)kcI18NDealloc;

@end

@interface UITextField (KCI18N)

@end

@interface UISearchBar (KCI18N)

@end