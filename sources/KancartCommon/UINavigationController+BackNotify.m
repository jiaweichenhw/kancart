//
//  UINavigationController+BackNotify.m
//  KanCart
//
//  Created by honestwalker on 1/16/14.
//
//

#import "UINavigationController+BackNotify.h"
#import "KCRuntimeSwizzle.h"

@implementation UINavigationController (BackNotify)

+ (void)prepare {
    [KCRuntimeSwizzle swizzle:UINavigationController.class
                      origSel:@selector(popViewControllerAnimated:)
                       newSel:@selector(kcPopViewControllerAnimated:)];

    [KCRuntimeSwizzle swizzle:UINavigationController.class
                      origSel:@selector(popToViewController:animated:)
                       newSel:@selector(kcPopToViewController:animated:)];

    [KCRuntimeSwizzle swizzle:UINavigationController.class
                      origSel:@selector(popToRootViewControllerAnimated:)
                       newSel:@selector(kcPopToRootViewControllerAnimated:)];
}

- (UIViewController *)kcPopViewControllerAnimated:(BOOL)animated {
    UIViewController *viewController = [self kcPopViewControllerAnimated:animated];
    if ([self isKindOfClass:NSClassFromString(@"UIMoreNavigationController")]) {
        [NSTimer scheduledTimerWithTimeInterval:0.5
                                         target:self
                                       selector:@selector(reloadMoreTableView)
                                       userInfo:nil
                                        repeats:NO];
    }
    if ([viewController respondsToSelector:NSSelectorFromString(@"backNotify:")]) {
        [viewController performSelectorOnMainThread:NSSelectorFromString(@"backNotify:")
                                         withObject:self
                                      waitUntilDone:NO];
    }
    return viewController;
}

- (void)reloadMoreTableView {
    UIViewController *moreListViewController = self.viewControllers[0];
    if ([moreListViewController.view isKindOfClass:UITableView.class]) {
        [(UITableView *)moreListViewController.view reloadData];
    }
}

- (NSArray *)kcPopToViewController:(UIViewController *)viewController animated:(BOOL)animated {
    NSArray *viewControllers = [self kcPopToViewController:viewController animated:animated];
    if ([self isKindOfClass:NSClassFromString(@"UIMoreNavigationController")]) {
        [NSTimer scheduledTimerWithTimeInterval:0.4
                                         target:self
                                       selector:@selector(reloadMoreTableView)
                                       userInfo:nil
                                        repeats:NO];
    }
    for (UIViewController *viewController in viewControllers) {
        if ([viewController respondsToSelector:NSSelectorFromString(@"backNotify:")]) {
            [viewController performSelectorOnMainThread:NSSelectorFromString(@"backNotify:")
                                             withObject:self
                                          waitUntilDone:NO];
        }
    }
    return viewControllers;
}

- (NSArray *)kcPopToRootViewControllerAnimated:(BOOL)animated {
    NSArray *viewControllers = [self kcPopToRootViewControllerAnimated:animated];
    if ([self isKindOfClass:NSClassFromString(@"UIMoreNavigationController")]) {
        [NSTimer scheduledTimerWithTimeInterval:0.4
                                         target:self
                                       selector:@selector(reloadMoreTableView)
                                       userInfo:nil
                                        repeats:NO];
    }
    for (UIViewController *viewController in viewControllers) {
        if ([viewController respondsToSelector:NSSelectorFromString(@"backNotify:")]) {
            [viewController performSelectorOnMainThread:NSSelectorFromString(@"backNotify:")
                                             withObject:self
                                          waitUntilDone:NO];
        }
    }
    return viewControllers;
}

@end
