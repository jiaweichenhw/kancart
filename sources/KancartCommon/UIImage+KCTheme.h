//
//  UIImage+KCTheme.h
//  KanCart
//
//  Created by honestwalker on 1/7/14.
//  Copyright (c) 2014 honestwalker.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (KCTheme)

- (UIImage *) imageWithTintColor:(UIColor *)tintColor;
- (UIImage *) imageWithGradientTintColor:(UIColor *)tintColor;
- (UIImage *) imageWithTintColor:(UIColor *)tintColor blendMode:(CGBlendMode)blendMode;

@end
