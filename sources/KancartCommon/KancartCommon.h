//
//  KancartCommon.h
//  KancartCommon
//
//  Created by Jiawei Chen on 14-9-28.
//  Copyright (c) 2014年 honestwalker.com. All rights reserved.
//

#ifndef KancartCommon_KancartCommon_h
#define KancartCommon_KancartCommon_h

#import "SFHFKeychainUtils.h"
#import "DataHelper.h"
#import "LocaleUtils.h"
#import "MarkupStripper.h"
#import "NSData-AES.h"
#import "NSData+Base64.h"
#import "NSDateFormatterAddition.h"
#import "NSDictionary+HTTPRequest.h"
#import "NSString+JSON.h"
#import "NSObject+JSON.h"
#import "NSString+Addition.h"
#import "ObserverableObject.h"
#import "KCRuntimeSwizzle.h"
#import "NSObject+KCFramework.h"
#import "KCI18NManager.h"
#import "NSString+KCI18N.h"
#import "UIView+KCI18N.h"
#import "UIDevice+UUIDIdentifier.h"
#import "UIImage+KCTheme.h"
#import "NSDate+Helper.h"
#import "EncryptMethod.h"
#import "CryptoUtil.h"
#import "UIImage+Blurring.h"
#import "UIImage+Enhancing.h"
#import "UIImage+Filtering.h"
#import "UIImage+Masking.h"
#import "UIImage+Reflection.h"
#import "UIImage+Resizing.h"
#import "UIImage+Rotating.h"
#import "UIImage+Saving.h"
#import "UIView+Screenshot.h"
#import "UIScrollView+Screenshot.h"
#import "UIImage+fixOrientation.h"
#import "NSURLRequest+Params.h"
#import "UINavigationController+BackNotify.h"
#import "UIColor+Hex.h"
#import "NSObject+ClassProperties.h"

#define NumValue(value, dValue) [DataHelper getNumberValue:value defaultValue:dValue]
#define StrValue(value, dValue) [DataHelper getStringValue:value defaultValue:dValue]
#define FloatValue(value, dValue) [DataHelper getFloatValue:value defaultValue:dValue]
#define DoubleValue(value, dValue) [DataHelper getDoubleValue:value defaultValue:dValue]
#define IntValue(value, dValue) [DataHelper getIntegerValue:value defaultValue:dValue]
#define BoolValue(value, dValue) [DataHelper getBoolValue:value defaultValue:dValue]
#define ArrayValue(value, dValue) [DataHelper getArrayValue:value defaultValue:dValue]
#define DictValue(value, dValue) [DataHelper getDictionaryValue:value defaultValue:dValue]

#endif
