//
//  NSObject+ClassProperties.h
//  KancartAPI
//
//  Created by Jiawei Chen on 14/12/1.
//  Copyright (c) 2014年 honestwalker.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (ClassProperties)

+ (NSDictionary *)classProperties;

@end
