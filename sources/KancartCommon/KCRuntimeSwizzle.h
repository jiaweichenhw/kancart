//
//  KCRuntimeSwizzle.h
//  KanCart
//
//  Created by honestwalker on 1/7/14.
//  Copyright (c) 2014 honestwalker.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KCRuntimeSwizzle : NSObject

+ (void)swizzle:(Class)c
        origSel:(SEL)origSel
         newSel:(SEL)newSel;

@end
