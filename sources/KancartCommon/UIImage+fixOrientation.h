//
//  UIImage+fixOrientation.h
//  KancartCommon
//
//  Created by Jiawei Chen on 14-10-16.
//  Copyright (c) 2014年 honestwalker.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (fixOrientation)

- (UIImage *)fixOrientation;

@end
