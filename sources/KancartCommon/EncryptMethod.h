//
//  EncryptMethod.h
//  CryptoUtil
//
//  Created by Jiawei Chen on 14-9-28.
//  Copyright (c) 2014年 honestwalker.com. All rights reserved.
//

#ifndef CryptoUtil_EncryptMethod_h
#define CryptoUtil_EncryptMethod_h

typedef enum {
    DES,
    TRPLE_DES,
    AES_256,
} ENCRYPT_METHOD;

#endif
