//
//  KCI18NManager.m
//  KanCart
//
//  Created by honestwalker on 1/15/14.
//  Copyright (c) 2014 honestwalker.com. All rights reserved.
//

#import "KCI18NManager.h"
#import <UIKit/UIKit.h>
#import "NSObject+KCFramework.h"
#import "NSString+KCI18N.h"
#import "UIView+KCI18N.h"

@interface KCI18NManager ()

@property (nonatomic, strong) NSMutableDictionary *objectToValue;

@end

@implementation KCI18NManager

@synthesize objectToValue;
@synthesize localizedDict;
@synthesize currentLanguage;
@synthesize baseDirectory;

+ (instancetype)sharedInstance {
    static KCI18NManager *instance;
	@synchronized(self) {
		if (!instance) {
			instance = [[KCI18NManager alloc] init];
            NSString *language = [[NSUserDefaults standardUserDefaults] stringForKey:@"DefaultLanguage"];
            instance.defaultLanguage = (language && language.length > 2)? language: @"EN";
            instance.currentLanguage = instance.defaultLanguage;
            instance.objectToValue = [[NSMutableDictionary alloc] initWithCapacity:300];
            [[NSNotificationCenter defaultCenter] addObserver:instance
                                                     selector:@selector(updateLanguageEvent:)
                                                         name:@"UpdateLanguageEvent"
                                                       object:nil];
		}
		return instance;
	}
}

+ (void)loadLocalizedDictionary:(NSString *)language {
    language = [language lowercaseString];
    KCI18NManager *i18n = [KCI18NManager sharedInstance];
    i18n.currentLanguage = language;
    NSArray *availableLanguages = [NSArray arrayWithObjects:@"English", @"Chinese", nil];
    NSDictionary *languagePair = [NSDictionary dictionaryWithObjectsAndKeys:
                                  @"English", @"en",
                                  @"France", @"fr",
                                  @"Portuguese", @"pt",
                                  @"Portuguese_Brazil", @"pt-br",
                                  @"Espanol", @"es",
                                  @"Deutsch", @"de",
                                  @"Chinese", @"zh",
                                  @"Traditional_Chinese", @"zh-hk",
                                  @"Traditional_Chinese", @"zh-tw",
                                  @"Italian", @"it", //Italian
                                  @"Japanese", @"ja",
                                  @"Korean", @"ko",
                                  @"Eestikeel", @"et", nil];
    NSString *targetLanguage = @"English";
    for (NSString *key in languagePair.allKeys) {
        NSRange range = [language rangeOfString:key];
        if (range.location == 0 && range.length == key.length) {
            targetLanguage = [languagePair valueForKey:key];
        }
    }
    if ([availableLanguages indexOfObject:targetLanguage] == NSNotFound) {
        targetLanguage = availableLanguages[0];
    }
    NSString *langPath = [[NSBundle mainBundle] pathForResource:targetLanguage ofType:@"strings"];
    i18n.localizedDict = [NSDictionary dictionaryWithContentsOfFile:langPath];
}

+ (NSString *)getValueForKey:(NSString *)key {
    if (!key) {
        // key 为nil, 直接返回 nil
        return nil;
    }
    KCI18NManager *i18n = [KCI18NManager sharedInstance];
    if(i18n.localizedDict == nil) {
        key.KCI18NName = [NSString stringWithString:key];
        return key;
    }
	NSString *result = [i18n.localizedDict objectForKey:key];
    //	NSLog(@"getLocalTextString, key:%@, result:%@", key, result);
	if(result == nil) {
        key.KCI18NName = [NSString stringWithString:key];
        return key;
    }
    result.KCI18NName = key;
    //如果是其他语言 根据key去找value；
    return result;
}

- (void)changeDefaultLanguage:(NSString *)language {
    self.defaultLanguage = language;
    [KCI18NManager loadLocalizedDictionary:self.defaultLanguage];
    NSUserDefaults *userdefaults = [NSUserDefaults standardUserDefaults];
    [userdefaults setObject:self.defaultLanguage forKey:@"DefaultLanguage"];
    [userdefaults synchronize];
}

- (void)updateLanguageEvent:(NSNotification *)notification {
    NSString *language = notification.userInfo[@"language"];
    [self changeDefaultLanguage:language];
    [self updateLanguage];
}

- (void)updateLanguage {
    for (NSString *key in self.objectToValue.allKeys) {
        id object = [self.objectToValue valueForKey:key];
        if (object && [object nonretainedObjectValue]) {
            UIView *obj = [object nonretainedObjectValue];
            if (obj) {
                [obj updateLanguage];
            }
        }
    }
}

- (void)replaceOld:(NSString *)oldObj withNew:(NSString *)newObj observer:(NSObject *)observer {
    if (newObj && newObj.KCI18NName) {
        NSValue *nonretainedValue = [NSValue valueWithNonretainedObject:observer];
        [self.objectToValue setValue:nonretainedValue forKey:observer.shortDescription];
    } else {
        [self removeObserver:observer];
    }
}

- (void)removeObserver:(NSObject *)observer {
    if (observer) {
        [self.objectToValue removeObjectForKey:observer.shortDescription];
    }
}

@end
