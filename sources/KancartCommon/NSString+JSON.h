//
//  NSString+JSON.h
//  core
//
//  Created by Jiawei Chen on 14-9-28.
//  Copyright (c) 2014年 honestwalker.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (JSON)

- (id)JSONValue;

@end
