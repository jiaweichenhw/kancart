//
//  KCI18NManager.h
//  KanCart
//
//  Created by honestwalker on 1/15/14.
//  Copyright (c) 2014 honestwalker.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#define I18N(s) [KCI18NManager getValueForKey:s]

@interface KCI18NManager : NSObject

@property (nonatomic, strong) NSDictionary *localizedDict;
@property (nonatomic, copy) NSString *defaultLanguage;
@property (nonatomic, copy) NSString *currentLanguage;
@property (nonatomic, copy) NSString *baseDirectory;

+ (instancetype)sharedInstance;

- (void)changeDefaultLanguage:(NSString *)language;

// 根据给定的语言 载入对应的语言文件
+ (void)loadLocalizedDictionary:(NSString *)langCode;

// 根据给定的key 获取特定的value
+ (NSString *)getValueForKey:(NSString *)key;

- (void)replaceOld:(NSString *)oldObj withNew:(NSString *)newObj observer:(NSObject *)observer;
- (void)removeObserver:(NSObject *)observer;

@end
