//
//  MarkupStripper.h
//  KanCart
//
//  Created by honestwalker on 2/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MarkupStripper : NSObject <NSXMLParserDelegate> {
@private
    NSMutableArray* _strings;
}

/**
 * Strips markup from the given string and returns the result.
 */
- (NSString*)parse:(NSString*)string;

@end