//
//  UIDevice+UUIDIdentifier.m
//  Drwine
//
//  Created by apple on 13-1-16.
//  Copyright (c) 2013年 HonestWalker. All rights reserved.
//

#import "UIDevice+UUIDIdentifier.h"
#import "SFHFKeychainUtils.h"
#import <objc/runtime.h>

@implementation UIDevice (UUIDIdentifier)

- (NSString *)deviceUUIDIdentifier {
    @synchronized (self) {
        if (!self.UUIDIdentifier) {
            // read from Keychains
            self.UUIDIdentifier = [SFHFKeychainUtils getPasswordForUsername:@"UUID"
                                                             andServiceName:[[NSBundle mainBundle] bundleIdentifier]
                                                                      error:nil];
            if (!self.UUIDIdentifier) {
                // if not in Keychains, create and save it.
                CFUUIDRef uuidRef = CFUUIDCreate(kCFAllocatorDefault);
                CFStringRef uuidStringRef= CFUUIDCreateString(kCFAllocatorDefault, uuidRef);
                CFRelease(uuidRef);
                self.UUIDIdentifier = [NSString stringWithString:(__bridge NSString*)uuidStringRef];
                CFRelease(uuidStringRef);
                
                [SFHFKeychainUtils storeUsername:@"UUID"
                                     andPassword:self.UUIDIdentifier
                                  forServiceName:[[NSBundle mainBundle] bundleIdentifier]
                                  updateExisting:YES
                                           error:nil];
            }
        }
        return self.UUIDIdentifier;
    }
}

static char UUIDIdentifierKey;

- (void)setUUIDIdentifier:(NSString *)UUIDIdentifier {
    // set
    objc_setAssociatedObject(self, &UUIDIdentifierKey, UUIDIdentifier, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (NSString *)UUIDIdentifier {
    // get
    NSString *associatedObject = objc_getAssociatedObject(self, &UUIDIdentifierKey);
    return associatedObject;
}

@end
