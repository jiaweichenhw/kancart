//
//  NSString+KCI18N.h
//  KanCart
//
//  Created by honestwalker on 1/15/14.
//  Copyright (c) 2014 honestwalker.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (KCI18N)

- (void)setKCI18NName:(NSString *)KCI18NName;

- (NSString *)KCI18NName;

@end
