//
//  NSString+JSON.m
//  core
//
//  Created by Jiawei Chen on 14-9-28.
//  Copyright (c) 2014年 honestwalker.com. All rights reserved.
//

#import "NSString+JSON.h"

@implementation NSString (JSON)

- (id)JSONValue {
    id obj = [NSJSONSerialization JSONObjectWithData:[self dataUsingEncoding:NSUTF8StringEncoding]
                                             options:NSJSONReadingMutableContainers
                                               error:nil];
    return obj;
}

@end
