//
//  EntityTables.h
//  KanCart
//
//  Created by honestwalker on 2/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EntityTables : NSObject {
    NSDictionary* _iso88591;
}

/**
 * Entity table for ISO 8859-1.
 */
@property (nonatomic, readonly) NSDictionary * iso88591;

@end


@interface EntityTables (TTSingleton)

// Access the singleton instance: [[TTEntityTables sharedInstance] <methods>]
+ (EntityTables *)sharedInstance;

@end