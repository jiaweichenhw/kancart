//
//  NSDictionary+HTTPRequest.h
//  KanCart
//
//  Created by Honestwalker MacVM on 7/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (HTTPRequest)

- (NSString *)getPostData;

@end
