//
//  NSObject+ClassProperties.m
//  KancartAPI
//
//  Created by Jiawei Chen on 14/12/1.
//  Copyright (c) 2014年 honestwalker.com. All rights reserved.
//

#import "NSObject+ClassProperties.h"
#import <objc/runtime.h>

@implementation NSObject (ClassProperties)

static const char * getPropertyType(objc_property_t property) {
    const char *attributes = property_getAttributes(property);
//    printf("attributes=%s\n", attributes);
    char buffer[1 + strlen(attributes)];
    strcpy(buffer, attributes);
    char *state = buffer, *attribute;
    while ((attribute = strsep(&state, ",")) != NULL) {
        if (attribute[0] == 'T' && attribute[1] != '@') {
            // it's a C primitive type:
            /*
             if you want a list of what will be returned for these primitives, search online for
             "objective-c" "Property Attribute Description Examples"
             apple docs list plenty of examples of what you get for int "i", long "l", unsigned "I", struct, etc.
             */
            NSString *type = [[NSString alloc] initWithUTF8String:attributes];
            NSRegularExpression *regular = [NSRegularExpression regularExpressionWithPattern:@"(?<=\\bT)(.*?)(?=\\,\\b)"
                                                                                     options:NSRegularExpressionCaseInsensitive
                                                                                       error:nil];
            NSArray *matches = [regular matchesInString:type options:0 range:NSMakeRange(0, type.length)];
            for (NSTextCheckingResult *result in matches) {
                type = [type substringWithRange:result.range];
            }
            return [type UTF8String];
        }
        else if (attribute[0] == 'T' && attribute[1] == '@' && strlen(attribute) == 2) {
            // it's an ObjC id type:
            return "id";
        }
        else if (attribute[0] == 'T' && attribute[1] == '@') {
            // it's another ObjC object type:
            NSString *type = [[NSString alloc] initWithUTF8String:attributes];
            NSRegularExpression *regular = [NSRegularExpression regularExpressionWithPattern:@"(?<=\").*?(?=\")"
                                                                                     options:NSRegularExpressionCaseInsensitive
                                                                                       error:nil];
            NSArray *matches = [regular matchesInString:type options:0 range:NSMakeRange(0, type.length)];
            for (NSTextCheckingResult *result in matches) {
                type = [type substringWithRange:result.range];
            }
            return [type UTF8String];
        }
    }
    return "";
}


+ (NSDictionary *)classProperties {
    NSMutableDictionary *results = [[NSMutableDictionary alloc] init];
    Class superClass = class_getSuperclass(self.class);
    while (superClass != [NSObject class]) {
        unsigned int outCount, i;
        objc_property_t *properties = class_copyPropertyList(superClass, &outCount);

        for (i = 0; i < outCount; i++) {
            objc_property_t property = properties[i];
            const char *propName = property_getName(property);
            if(propName) {
                const char *propType = getPropertyType(property);
                NSString *propertyName = [NSString stringWithUTF8String:propName];
                NSString *propertyType = [NSString stringWithUTF8String:propType];
                [results setValue:propertyType forKey:propertyName];
            }
        }
        free(properties);

        superClass = class_getSuperclass(superClass);
    }
    unsigned int outCount, i;
    objc_property_t *properties = class_copyPropertyList(self.class, &outCount);

    for (i = 0; i < outCount; i++) {
        objc_property_t property = properties[i];
        const char *propName = property_getName(property);
        if(propName) {
            const char *propType = getPropertyType(property);
            NSString *propertyName = [NSString stringWithUTF8String:propName];
            NSString *propertyType = [NSString stringWithUTF8String:propType];
            [results setValue:propertyType forKey:propertyName];
        }
    }
    free(properties);


    // returning a copy here to make sure the dictionary is immutable
    return [NSDictionary dictionaryWithDictionary:results];
}

@end
