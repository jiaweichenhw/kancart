//
//  UIDevice+UUIDIdentifier.h
//  Drwine
//
//  Created by apple on 13-1-16.
//  Copyright (c) 2013年 HonestWalker. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIDevice (UUIDIdentifier)

- (NSString *)deviceUUIDIdentifier;

@end
