//
//  NSObject+KCFramework.h
//  KanCart
//
//  Created by honestwalker on 1/21/14.
//  Copyright (c) 2014 honestwalker.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (KCFramework)

- (NSString *)shortDescription;

- (void)setKcUserInfo:(NSMutableDictionary *)kcUserInfo;
- (NSMutableDictionary *)kcUserInfo;


@end
