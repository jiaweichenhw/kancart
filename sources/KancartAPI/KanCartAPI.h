//
//  KanCartAPI.h
//  Drwine
//
//  Created by honestwalker on 9/27/12.
//  Copyright (c) 2012 HonestWalker. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KancartAPIDelegate.h"
#import "EncryptMethod.h"

@class ASIHTTPRequest;
@class APIRequestHelper;

@interface KanCartAPI : NSObject {

}

/**
 * 语言（默认英语）
 */
@property (nonatomic, copy) NSString *API_Language;

/**
 * 币种（默认美元）
 */
@property (nonatomic, copy) NSString *API_Currency;

/**
 * 用户登录标识
 */
@property (nonatomic, copy) NSString *API_SessionKey;

@property (nonatomic) ENCRYPT_METHOD API_Encrypt_Method;

@property (nonatomic, strong) id<KancartAPIDelegate> delegate;

/** 获取KanCartAPI的单一实例
 * @return id KanCartAPI的单一实例
 */
+ (instancetype)getInstance;
- (void)reload;

/** 发起一次HTTP请求
 * @param requestHelper        请求辅助类
 * @return ASIHTTPRequest 本次HTTP请求对象
 */
+ (ASIHTTPRequest *)requestWithHelper:(APIRequestHelper *)requestHelper;

- (NSString *)appKeyForRequestHelper:(APIRequestHelper *)requestHelper;
- (NSString *)appSecretForRequestHelper:(APIRequestHelper *)requestHelper;
- (NSString *)methodNameForHelper:(NSString *)helperName;

@end