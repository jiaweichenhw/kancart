//
//  ServiceUrlCollection.h
//  Drwine
//
//  Created by honestwalker on 9/28/12.
//  Copyright (c) 2012 HonestWalker. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KancartAPIDelegate.h"

@interface ServiceUrlCollection : NSObject {
    NSString *httpUrl;
    NSString *httpsUrl;
    NSMutableDictionary *serviceUrls;
}

@property (nonatomic, copy) NSString *httpUrl;
@property (nonatomic, copy) NSString *httpsUrl;
@property (nonatomic, strong) NSMutableDictionary *serviceUrls;

- (id)initWithDelegate:(id<KancartAPIDelegate>)delegate configFilePath:(NSString *)filePath;

@end

@interface ServiceUrl : NSObject

@property (nonatomic, copy) NSString *methodName;
@property (nonatomic, copy) NSString *url;
@property (nonatomic) BOOL needLogin;
@property (nonatomic) BOOL useSSL;

@end