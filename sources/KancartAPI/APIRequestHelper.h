//
//  APIRequestHelper.h
//  Drwine
//
//  Created by honestwalker on 9/28/12.
//  Copyright (c) 2012 HonestWalker. All rights reserved.
//

#import <Foundation/Foundation.h>

@class MBProgressHUD;
@class KCProgressView;

@interface APIRequestHelper : NSObject

@property (nonatomic, strong) NSString *methodName;
@property (nonatomic, strong) MBProgressHUD *hudView;
@property (nonatomic, strong) KCProgressView *progressView;
@property (nonatomic, strong) NSMutableDictionary *responseData;
@property (nonatomic, strong) NSObject *notificationObserver;
@property (nonatomic, strong) NSMutableDictionary *orginalResponseData;
@property (nonatomic, strong) NSDictionary *userInfo;
@property (nonatomic) BOOL isServiceRequest;

+ (instancetype)getHelper;

- (void)setup;
- (void)sendRequest;
- (NSMutableDictionary *)getRequestParams;
- (NSMutableDictionary *)getRequestFiles;
- (void)handleResponseData:(NSMutableDictionary *)responseInfo;

@end
