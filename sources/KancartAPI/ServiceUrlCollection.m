//
//  ServiceUrlCollection.m
//  Drwine
//
//  Created by honestwalker on 9/28/12.
//  Copyright (c) 2012 HonestWalker. All rights reserved.
//

#import "ServiceUrlCollection.h"
#import "DataHelper.h"
#import "NSString+Addition.h"

@implementation ServiceUrl

@synthesize url;
@synthesize needLogin;
@synthesize useSSL;

@end

@implementation ServiceUrlCollection

@synthesize httpUrl;
@synthesize httpsUrl;
@synthesize serviceUrls;

- (id)initWithDelegate:(id<KancartAPIDelegate>)delegate
        configFilePath:(NSString *)filePath {
    self = [super init];
    if (self) {
        NSError *error = nil;
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"(kancart|\\.)"
                                                                               options:NSRegularExpressionCaseInsensitive
                                                                                 error:&error];

        NSMutableDictionary *serviceAPIMapConfiguration = [NSMutableDictionary dictionaryWithContentsOfFile:filePath];

        self.httpUrl = [serviceAPIMapConfiguration objectForKey:@"HttpUrl"];
        self.httpsUrl = [serviceAPIMapConfiguration objectForKey:@"HttpsUrl"];

        if ([self.httpUrl isEqualToString:@"STORE_PLUGIN_URL"]) {
            self.httpUrl = [delegate getAPIUrl];
        }

        if ([self.httpsUrl isEqualToString:@"STORE_PLUGIN_URL"]) {
            self.httpUrl = [delegate getAPIUrl];
        }
        NSDictionary *maps = [serviceAPIMapConfiguration objectForKey:@"APIMap"];

        self.serviceUrls = [[NSMutableDictionary alloc] initWithCapacity:maps.count];

        NSArray *methodKeys = maps.allKeys;
        for (NSString *methodName in methodKeys) {
            NSMutableDictionary *attributes = maps[methodName];

            ServiceUrl *serviceUrl = [[ServiceUrl alloc] init];
            serviceUrl.methodName = methodName;

            // 请求的url链接
            serviceUrl.url = [DataHelper getStringValue:[attributes objectForKey:@"url"]
                                           defaultValue:nil];

            // 是否使用 HTTPS (SSL)
            serviceUrl.useSSL = [DataHelper getBoolValue:[attributes objectForKey:@"useSSL"]
                                            defaultValue:NO];

            // 是否需要登录
            serviceUrl.needLogin = [DataHelper getBoolValue:[attributes objectForKey:@"needLogin"]
                                               defaultValue:NO];

            if ([NSString IsNilOrEmpty:serviceUrl.url]) {
                // 如果获取到的url 为nil, 那么根据是否使用 SSL 来设置默认的url
                if (serviceUrl.useSSL == NO) {
                    serviceUrl.url = self.httpUrl;
                } else {
                    serviceUrl.url = self.httpsUrl;
                }
            } else if ([serviceUrl.url isEqualToString:@"MOBILE_SITE_API_URL"]) {
                serviceUrl.url = @"MOBILE_SITE_API_URL";
            }
            NSString *helperName = [regex stringByReplacingMatchesInString:methodName
                                                               options:0
                                                                 range:NSMakeRange(0, methodName.length)
                                                          withTemplate:@""];

            // 用 ServiceUrl 对象替换 Dictionary
            [self.serviceUrls setValue:serviceUrl forKey:[[NSString stringWithFormat:@"%@helper", helperName] lowercaseString]];
        }
    }
    return self;
}

@end
