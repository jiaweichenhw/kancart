//
//  KanCartAPI.m
//  Drwine
//
//  Created by honestwalker on 9/27/12.
//  Copyright (c) 2012 HonestWalker. All rights reserved.
//

#import "KanCartAPI.h"
#import "ServiceUrlCollection.h"
#import "APIRequestHelper.h"
#import <ASIHTTPRequest/ASIFormDataRequest.h>

#import "KancartCommon.h"

@interface KanCartAPI ()
/**
 * app service 对应的url集合
 */
@property (nonatomic, strong) NSMutableDictionary *appServiceUrls;

/**
 * kancart service 对应的url集合
 */
@property (nonatomic, strong) NSMutableDictionary *kancartServiceUrls;

/**
 * 请求方法（默认POST）
 */
@property (nonatomic, copy) NSString *API_RequestMethod;

/**
 * 返回的值类型(默认JSON)
 */
@property (nonatomic, copy) NSString *API_Format;

/**
 * 用户会话ID
 */
@property (nonatomic, copy) NSString *API_SessionID;

/**
 * api版本
 */
@property (nonatomic, copy) NSString *API_Version;

/**
 * 日期格式化器
 */
@property (nonatomic, strong) NSDateFormatter *dateFormatter;

@property (nonatomic, strong) NSMutableArray *requestHelpers;
@property (nonatomic, strong) NSMutableArray *requests;

/** 获取指定配置文件的 ServiceUrls
 * @return NSMutableDictionary 配置文件的 ServiceUrls
 */
- (NSMutableDictionary *)loadServiceUrlsWithFilePath:(NSString *)filePath;

/** 载入 APP ServiceUrls
 * @return nil
 */
- (void)loadAppServiceUrlsWithFilePath:(NSString *)filePath;

/** 加入系统级参数
 * @param param     原数据对象
 * @return 无返回值
 */
- (void)putSystemParam:(NSMutableDictionary *)param
         requestHelper:(APIRequestHelper *)requestHelper;

/** 生成一个HTTP请求对象
 * @param timeout           超时设置(单位:s)
 * @param retryOnTimeout    超时重试次数
 * @param url               请求的url
 * @param delegate          委托对象
 * @param userInfo          该请求附带的用户信息
 * @return ASIHTTPRequest 本次请求对象
 */
- (ASIHTTPRequest *)createRequest:(NSInteger)timeout
                   retryOnTimeout:(int)retryOnTimeout
                         delegate:(id)delegate
                              url:(NSString *)url
                         userInfo:(NSDictionary *)userInfo;

/** HTTP请求执行失败，处理http请求执行失败信息
 * @param request 执行完成的HTTP请求
 * @return 没有返回值
 */
- (void)requestFailed:(ASIHTTPRequest *)request;

/**
 * HTTP请求执行成功，处理成功和失败两种情况
 * @param request         执行完成的HTTP请求
 * @return 没有返回值
 * @retval void
 */
- (void)requestFinished:(ASIHTTPRequest *)request;

@end

@implementation KanCartAPI

@synthesize appServiceUrls;
@synthesize kancartServiceUrls;
@synthesize API_Language;
@synthesize API_Currency;
@synthesize API_RequestMethod;
@synthesize API_Format;
@synthesize API_SessionKey;
@synthesize API_SessionID;
@synthesize API_Version;
@synthesize dateFormatter;
@synthesize API_Encrypt_Method;
@synthesize requestHelpers;
@synthesize requests;

- (instancetype)init {
	self = [super init];
    if (self) {
        self.API_Language = @"EN";
        self.API_Currency = @"USD";
        self.API_RequestMethod = @"POST";
        self.API_Format = @"JSON";
        self.API_SessionKey = @"";
        self.API_Version = @"1.1";
        self.API_SessionID = @"";
        self.API_Encrypt_Method = AES_256;
        
        self.dateFormatter = [[NSDateFormatter alloc] init];
        [self.dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
        [self.dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        self.requestHelpers = [[NSMutableArray alloc] init];
        self.requests = [[NSMutableArray alloc] init];
    }
	return self;
}

- (void)setDelegate:(id<KancartAPIDelegate>)aDelegate {
    _delegate = aDelegate;
    NSString *path = [[NSBundle mainBundle] pathForResource:@"KanCartServiceAPIMap" ofType:@"plist"];
    self.kancartServiceUrls = [self loadServiceUrlsWithFilePath:path];

    NSString *appServiceFilePath = [[NSBundle mainBundle] pathForResource:@"APIMap" ofType:@"plist"];
    [self loadAppServiceUrlsWithFilePath:appServiceFilePath];
}

- (void)loadAppServiceUrlsWithFilePath:(NSString *)filePath {
    self.appServiceUrls = [self loadServiceUrlsWithFilePath:filePath];
}

- (NSMutableDictionary *)loadServiceUrlsWithFilePath:(NSString *)filePath {
    ServiceUrlCollection *serviceUrlCollection = [[ServiceUrlCollection alloc] initWithDelegate:self.delegate configFilePath:filePath];
    return serviceUrlCollection.serviceUrls;
}

- (void)reload {
    NSString *path = [[NSBundle mainBundle] pathForResource:@"KanCartServiceAPIMap" ofType:@"plist"];
    self.kancartServiceUrls = [self loadServiceUrlsWithFilePath:path];
    
    NSString *appServiceFilePath = [[NSBundle mainBundle] pathForResource:@"APIMap" ofType:@"plist"];
    [self loadAppServiceUrlsWithFilePath:appServiceFilePath];
}

- (NSString *)methodNameForHelper:(NSString *)helperName {
    ServiceUrl *url = self.kancartServiceUrls[[helperName lowercaseString]];
    if (!url) {
        url = self.appServiceUrls[[helperName lowercaseString]];
    }
    return url.methodName;
}

/**
 * 获取KanCartAPI的单一实例
 * @return id KanCartAPI的单一实例
 */
+ (instancetype)getInstance {
	static KanCartAPI *api;
	@synchronized(self) {
		if (!api) {
			api = [[KanCartAPI alloc] init];
		}
		return api;
	}
}

- (NSString *)appKeyForRequestHelper:(APIRequestHelper *)requestHelper {
    BOOL isServiceRequest = requestHelper.isServiceRequest;
    
    if ([self.delegate isAgentMode] && !isServiceRequest) {
        return [self.delegate getAgentAppKey];
    }
    return [self.delegate getAppKey];
}

- (NSString *)appSecretForRequestHelper:(APIRequestHelper *)requestHelper {
    BOOL isServiceRequest = requestHelper.isServiceRequest;
    
    if ([self.delegate isAgentMode] && !isServiceRequest) {
        return [self.delegate getAgentAppSecret];
    }
    return [self.delegate getAppSecret];
}

/**
 * 通过计算返回KanCartAPI请求的签名
 * 第一步：移除参数中的key为sign的值(以防万一)
 * 第二步：按字典顺序将key排序(不区分大小写)
 * 第三步：按Key1Value1Key2Value2Key3Value3....的顺序将所有的键值按顺序拼接成一个字符串
 * 第四步：在该字符串末尾加上app_secret
 * 第五步：计算该拼接字符串的md5值(大写)
 * @param parameters    该请求的所有参数(不包括参数sign)
 * @param secret        App_Secret
 * @return NSString 该拼接字符串的md5值(大写)
 */
- (NSString *)CreateSign:(NSMutableDictionary *)parameters
                  secret:(NSString *)secret {
	// 第一步：移除参数中的key为sign的值 
	[parameters removeObjectForKey:@"sign"];
    
	// 第二步：按字典顺序将key排序(不区分大小写)
	NSArray *sortedArray = [[parameters allKeys] sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)];
    
	// 第三步：按Key1Value1Key2Value2Key3Value3....的顺序将所有的键值按顺序拼接成一个字符串
	NSMutableString *query= [[NSMutableString alloc] init];
	for (NSString *key in sortedArray) {
		NSString *value = [[parameters objectForKey:key] description];
		[query appendString:key];
        if (![NSString IsNilOrEmpty:value]) {
            [query appendString:value];
        }
	}
    // 第四步：在该字符串末尾加上app_secret
	[query appendString:secret];
//	DLog(@"query:\r\n%@", query);
    
	// 第五步：计算该拼接字符串的md5值(大写)
	return [CryptoUtil md5:query];
}

/**
 * 组装普通文本请求参数
 * @param parameters    Key-Value形式请求参数字典(不包括参数sign)
 * @return NSString 返回URL编码后的请求数据
 */
- (NSString *)PostData:(NSMutableDictionary *)parameters {
	NSMutableString *postData = [[NSMutableString alloc] init];
	BOOL hasPara = NO;
	NSEnumerator *enumerator = [parameters keyEnumerator];
	id key;
	while ((key = [enumerator nextObject])) {
		NSString *name = [key description];
		NSString *value = [[parameters objectForKey:key] description];
		// 忽略参数名或参数值为空的参数
		if(![NSString IsNilOrEmpty:name])
		{
			if (hasPara) {
				[postData appendString:@"&"];
			}
			[postData appendString:name];
			[postData appendString:@"="];
			[postData appendString:[value stringByURLEncodingStringParameter]];
			hasPara = YES;
		}
	}
	return [postData description];
}

#pragma mark - KanCartAPI:param:object:delegate:(id)delegate
/**
 * 发起一次HTTP请求
 * @param method    请求方法
 * @param delegate  委托对象
 * @return ASIHTTPRequest   本次请求对象
 */
- (ASIHTTPRequest *)KanCartAPI:(APIRequestHelper *)requestHelper
                       delegate:(id)delegate {
    NSString *helperName = [NSStringFromClass([requestHelper class]) lowercaseString];
    // 添加系统参数
    ServiceUrl *serviceUrl = [self.appServiceUrls objectForKey:helperName];
    
    if (serviceUrl == nil) {
        serviceUrl = [self.kancartServiceUrls objectForKey:helperName];
    }
    if (serviceUrl == nil) {
        // TODO 没有对应的请求
    }
    
	NSMutableDictionary *userInfo = [[NSMutableDictionary alloc] initWithCapacity:1];
    [userInfo setValue:requestHelper forKey:@"requestHelper"];
    
	ASIHTTPRequest *request = [self createRequest:30
                                   retryOnTimeout:1
                                         delegate:delegate
                                              url:serviceUrl.url
                                         userInfo:userInfo];
    
    
    // 添加请求相关信息
    NSMutableDictionary *params = [requestHelper getRequestParams];
    if (serviceUrl.needLogin) {
        [params setValue:[delegate getCurrentUsername] forKey:@"uname"];
    }
    
    [self putSystemParam:params requestHelper:requestHelper];
    
    NSMutableDictionary *files = [requestHelper getRequestFiles];
    
    NSString *temppara = [self PostData:params];
    //const char *cStr = [temppara UTF8String];//编码，以便post写入
    
    [request setRequestMethod:API_RequestMethod];
    if ([API_RequestMethod isEqualToString:@"POST"]) {
        [request setURL:[NSURL URLWithString:serviceUrl.url]];
        for (NSString *key in [params allKeys]) {
            [(ASIFormDataRequest *)request addPostValue:[params objectForKey:key] forKey:key];
        }
        
        // 添加文件数据
        for (NSString *key in [files allKeys]) {
            NSString *path = files[key];
            UIImage *img = [UIImage imageWithContentsOfFile:path];
            [(ASIFormDataRequest *)request setData:UIImagePNGRepresentation(img)
                                      withFileName:files[key]
                                    andContentType:@"image/png"
                                            forKey:key];
        }
    } else {
        NSString *para =[NSString stringWithFormat:@"%@?%@", serviceUrl.url, temppara];
        [request setURL:[NSURL URLWithString:para]];
    }
    
    [request setDefaultResponseEncoding:NSUTF8StringEncoding];//NSISOLatin1StringEncoding
    [request startAsynchronous];
    /*//Making synchronous requests
     [request start];
     NSError *error = [request error];
     
     if (!error) {
     NSString *response = [request responseString];
     } else {
     DLog ( @"Something went wrong" );
     }*/
    
    if (serviceUrl.needLogin) {
        [self.requests addObject:request];
        if (![self.requestHelpers containsObject:requestHelper]) {
            [self.requestHelpers addObject:requestHelper];
        }
    }
    
    return request;
}

/** 
 * 发起一次HTTP请求
 * @param requestHelper   请求辅助类
 * @return ASIHTTPRequest 本次HTTP请求对象
 */
+ (ASIHTTPRequest *)requestWithHelper:(APIRequestHelper *)requestHelper {
    
    // 给观察者  添加通知观察着
    [requestHelper.notificationObserver addNotificationObserver:requestHelper.methodName
                                                         object:requestHelper];
    
    // 调用 KanCartAPI 发送请求
    return [[KanCartAPI getInstance] KanCartAPI:requestHelper delegate:nil];
}

/** 
 * 加入系统级参数
 * @param param     原数据对象
 * @return 无返回值
 */
- (void)putSystemParam:(NSMutableDictionary *)param
         requestHelper:(APIRequestHelper *)requestHelper {
    NSString *appKey = [[KanCartAPI getInstance] appKeyForRequestHelper:requestHelper];
    NSString *appSecret = [[KanCartAPI getInstance] appSecretForRequestHelper:requestHelper];
    
    [param setObject:@"ios" forKey:@"client"];
    [param setObject:[self.delegate getLanguage] forKey:@"language"];
    [param setObject:[self.delegate getCurrency] forKey:@"currency"];
    [param setObject:appKey forKey:@"app_key"];
    [param setValue:API_SessionKey forKey:@"session"];
    [param setObject:[self.delegate getCurrentDeviceId] forKey:@"device_id"];
    [param setObject:[dateFormatter stringFromDate:[NSDate date]] forKey:@"timestamp"]; // 时间戳，格式为yyyy-mm-dd hh:mm:ss，例如：2008-01-25 20:23:30。HonestWalkerAPI服务端允许客户端请求时间误差为10分钟。
    [param setObject:API_Format forKey:@"format"]; // 响应格式。xml,json。
    [param setObject:API_Version forKey:@"v"]; // API版本号
    [param setObject:@"md5" forKey:@"sign_method"]; // 签名类型
    
    // 计算签名
    [param setObject:[self CreateSign:param secret:appSecret] forKey:@"sign"];
    
    if (self.API_Encrypt_Method == AES_256) {
        NSString *encryptedAppKey = [CryptoUtil AES_256:appKey
                                    encryptYESDecryptNO:YES
                                                    key:appSecret];
        [param setValue:encryptedAppKey forKey:@"app_key"];
    }
}

#pragma mark - createRequest:retryOnTimeout:delegate:url:userInfo:
/**
 * 生成一个HTTP请求对象
 * @param timeout           超时设置(单位:s)
 * @param retryOnTimeout    超时重试次数
 * @param url               请求的url
 * @param delegate          委托对象
 * @param userInfo          该请求附带的用户信息
 * @return ASIHTTPRequest   本次请求对象
 */
- (ASIHTTPRequest *)createRequest:(NSInteger)timeout
                   retryOnTimeout:(int)retryOnTimeout
                         delegate:(id)delegate
                              url:(NSString *)url
                         userInfo:(NSDictionary *)userInfo {
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:url]];
    [request setShouldContinueWhenAppEntersBackground:YES];
    [request setTimeOutSeconds:timeout];
	[request setNumberOfTimesToRetryOnTimeout:retryOnTimeout];
    if (delegate) {
        [request setDelegate:delegate];
    } else {
        [request setDelegate:self];
    }
	[request setCachePolicy:ASIDoNotReadFromCacheCachePolicy];
	[request setValidatesSecureCertificate:NO];
    [request setUserInfo:userInfo];
    [request setAllowCompressedResponse:YES];
    
    NSDictionary *settings = [NSDictionary dictionaryWithContentsOfFile:[@"~/Library/Preferences/Cookies.plist" stringByExpandingTildeInPath]];
    settings = [NSMutableDictionary dictionaryWithDictionary:settings];
    
    NSMutableArray *cookieArray = [settings objectForKey:@"cookies"];
    NSMutableArray *nsCookieArray = [[NSMutableArray alloc] initWithCapacity:10];
    for (int i = 0; i < cookieArray.count; i++) {
        NSHTTPCookie *cookie = [NSHTTPCookie cookieWithProperties:[cookieArray objectAtIndex:i]];
        [nsCookieArray addObject:cookie];
    }
    [request setRequestCookies:nsCookieArray];
    
    return request;
}

/**
 * 移除JSON字符串前面的异常字符, 包括BOM和服务器异常输出的内容
 * @param string
 * @return responseString
 */
- (NSString *)removeHeaderExpectionCharacters:(NSString *)responseString {
    NSRange range = [responseString rangeOfString:@"{"];
    if (range.location == NSNotFound) {
        // 找不到"{", 返回数据不符合文档描述的正常JSON字符串, 返回正确的异常结果
        NSString *result = @"fail";
        NSString *code = @"0";
        NSString *errmsg = @"请求异常, 错误码0x0001";
        return [NSDictionaryOfVariableBindings(result, code, errmsg) JSONRepresentation];
    }
    if (range.location != NSNotFound && range.location > 0) {
        return [responseString substringFromIndex:range.location];
    }
    return responseString;
}

/**
 * HTTP请求执行成功，处理成功和失败两种情况
 * @param request   执行完成的HTTP请求
 * @return 没有返回值
 */
- (void)requestFinished:(ASIHTTPRequest *)request {
    // 请求所附带的Helper
    APIRequestHelper *requestHelper = request.userInfo[@"requestHelper"];

    // 解析返回结果
	NSData *data = request.responseData;
	NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    responseString = [self removeHeaderExpectionCharacters:responseString];

	NSMutableDictionary *response = [responseString JSONValue];

    // 请求结果
    NSString *result = [StrValue(response[@"result"], @"fail") lowercaseString];
    NSString *errmsg = StrValue(response[@"errmsg"], nil);
    NSString *code = StrValue(response[@"code"], @"1");
	NSMutableDictionary *info = DictValue(response[@"info"], nil);

    ServiceUrl *serviceUrl = [self.appServiceUrls objectForKey:requestHelper.methodName];
    if (serviceUrl == nil) {
        serviceUrl = [self.kancartServiceUrls objectForKey:requestHelper.methodName];
    }
    
	if ([result isEqualToString:@"success"]) {
        // 如果请求需要登陆，才从队列中删除，否则请求不在队列中
        if (serviceUrl.needLogin) {
            [self.requests removeObject:request];
            [self.requestHelpers removeObject:requestHelper];
        }
        requestHelper.orginalResponseData = info;
        // 处理成功后相应的数据解析，有子类自行处理
		[requestHelper handleResponseData:info];
        
        // save cookies
        NSArray *cookies = [request responseCookies];
        NSMutableArray *cookiesArray = [[NSMutableArray alloc] initWithCapacity:10];
        for (int i = 0; i < cookies.count; i++) {
            [cookiesArray addObject:[[cookies objectAtIndex:0] properties]];
        }
        [self saveCookie:cookiesArray];
        
        // save login status
        if ([requestHelper isKindOfClass:NSClassFromString(@"UserLoginHelper")]) {
            if ([[requestHelper.responseData objectForKey:@"action"] isEqualToString:@"login"]) {
                self.API_SessionKey = @"hasLogin";
            }
        }

		// 处理完成后发出成功通知
		[[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@_%@", requestHelper.methodName, @"succeeded"]
                                                            object:requestHelper
                                                          userInfo:nil];
	} else {
        // 如果自动登陆失败，则跳转到登陆页面
//        if ([requestHelper isKindOfClass:NSClassFromString(@"UserLoginHelper")]) {
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"AutoLoginFailedEvent"
//                                                                object:requestHelper];
//        }
        if (!errmsg) {
            errmsg = @"异常错误";
        }

        // TODO: 如果出现 2 错误时, 自动重新登陆
        if (code.integerValue == 2) {
            for (ASIHTTPRequest *request in self.requests) {                
                [request clearDelegatesAndCancel];
            }
            
            [self.requests removeAllObjects];
            
            // LoginSessionInvalidEvent need handler
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"LoginSessionInvalidEvent"
                                                                object:nil
                                                              userInfo:nil];
            return;
        } else {
            if (serviceUrl.needLogin) {
                [self.requestHelpers removeObject:requestHelper];
                [self.requests removeObject:request];
            }
        }
        // 请求错误，发送错误通知
        if (requestHelper.progressView != nil) {
//            [requestHelper.progressView setErrorMsg:err_msg];
        }
        NSString *notificationName = [NSString stringWithFormat:@"%@_%@", requestHelper.methodName, @"failed"];
        [[NSNotificationCenter defaultCenter] postNotificationName:notificationName
                                                            object:requestHelper
                                                          userInfo:NSDictionaryOfVariableBindings(result, code, errmsg)];
	}
    [requestHelper.notificationObserver removeNoticiationObserver:requestHelper.methodName
                                                           object:requestHelper];
}

/**
 * HTTP请求执行失败，处理http请求执行失败信息
 * @param request 执行完成的HTTP请求
 * @return 没有返回值
 */
- (void)requestFailed:(ASIHTTPRequest *)request {
	APIRequestHelper *requestHelper = [[request userInfo] objectForKey:@"requestHelper"];
    NSString *result = @"fail";
    NSString *code = @"1";
    NSString *errmsg = @"网络连接异常, 请稍后重试!";

    if (requestHelper.progressView != nil) {
//            [requestHelper.progressView setErrorMsg:@"Network Error"];
    }
    NSString *notificationName = [NSString stringWithFormat:@"%@_%@", requestHelper.methodName, @"error"];
    [[NSNotificationCenter defaultCenter] postNotificationName:notificationName
                                                        object:requestHelper
                                                      userInfo:NSDictionaryOfVariableBindings(result, code, errmsg)];
    [requestHelper.notificationObserver removeNoticiationObserver:requestHelper.methodName
                                                           object:requestHelper];
}

#pragma mark - Save Cookies
/**
 * 保存请求的cookies (仅在需通过cookie维持回话时使用)
 * @param cookies
 * @return 没有返回值
 */
- (void)saveCookie:(NSMutableArray *)cookies {
    if (cookies.count > 0) {
        NSMutableDictionary *newCookies = [[NSMutableDictionary alloc] init];
        for (NSDictionary *cookie in cookies) {
            if (![NSString IsNilOrEmpty:cookie[@"Name"]]) {
                [newCookies setValue:cookie forKey:cookie[@"Name"]];
            }
        }
        NSDictionary *settings = [NSDictionary dictionaryWithContentsOfFile:[@"~/Library/Preferences/Cookies.plist" stringByExpandingTildeInPath]];
        settings = [NSMutableDictionary dictionaryWithDictionary:settings];
        [settings setValue:newCookies.allValues forKey:@"cookies"];
        [settings writeToFile:[@"~/Library/Preferences/Cookies.plist" stringByExpandingTildeInPath] atomically:YES];
    }
}

@end