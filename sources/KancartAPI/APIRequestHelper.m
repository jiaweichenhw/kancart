//
//  APIRequestHelper.m
//  Drwine
//
//  Created by honestwalker on 9/28/12.
//  Copyright (c) 2012 HonestWalker. All rights reserved.
//

#import "APIRequestHelper.h"
#import "ServiceUrlCollection.h"
#import "KanCartAPI.h"

@interface APIRequestHelper ()

@end

@implementation APIRequestHelper

@synthesize methodName;
@synthesize hudView;
@synthesize responseData;
@synthesize orginalResponseData;
@synthesize notificationObserver;
@synthesize isServiceRequest;

+ (instancetype)getHelper {
    return [[self.class alloc] init];
}

- (instancetype)init {
    self = [super init];
    if (self) {
        self.responseData = [[NSMutableDictionary alloc] init];
        self.isServiceRequest = NO;
        self.methodName = [[KanCartAPI getInstance] methodNameForHelper:NSStringFromClass(self.class)];
        [self setup];
    }
    return self;
}

- (void)setup {
}

- (void)sendRequest {
    [KanCartAPI requestWithHelper:self];
}

- (NSMutableDictionary *)getRequestParams {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setValue:self.methodName forKey:@"method"];
    return params;
}

- (NSMutableDictionary *)getRequestFiles {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    return params;
}

- (void)handleResponseData:(NSMutableDictionary *)responseInfo {
    // 父类不做任何操作
}

- (void)dealloc {
    self.notificationObserver = nil;
}

@end