//
//  KancartAPIDelegate.h
//  KancartAPI
//
//  Created by Jiawei Chen on 14-9-28.
//  Copyright (c) 2014年 honestwalker.com. All rights reserved.
//

#ifndef KancartAPI_KancartAPIDelegate_h
#define KancartAPI_KancartAPIDelegate_h

@protocol KancartAPIDelegate <NSObject>

- (NSString *)getCurrentUsername;
- (NSString *)getCurrentPassword;
- (NSString *)getCurrentDeviceId;


- (NSString *)getAPIUrl;

- (NSString *)getServiceAppKey;
- (NSString *)getServiceAppSecret;
- (NSString *)getAgentAppKey;
- (NSString *)getAgentAppSecret;
- (NSString *)getAppKey;
- (NSString *)getAppSecret;
- (NSString *)getCurrencyCode;
- (NSString *)getLanguageCode;
- (BOOL)isAgentMode;

@end


#endif
