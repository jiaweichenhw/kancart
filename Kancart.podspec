Pod::Spec.new do |s|
  s.name         = "Kancart"
  s.version      = "1.0.0"
  s.summary      = "Kancart"

  s.description  = <<-DESC
                   A longer description of Kancart in Markdown format.
                   DESC

  s.homepage     = "http://www.kancart.com"
  s.license      = "MIT (example)"

  s.authors            = { "Jiawei Chen" => "jiawei.maple@gmail.com" }

  # s.platform     = :ios, "7.0"

  s.source       = { :git => "http://git.jmaple.com/Kancart.git", :tag => "1.0.0" }

  s.source_files  = "sources", "sources/**/*.{h,m,mm,c}"
  s.public_header_files = "sources/**/*.h"

  s.requires_arc = true
  s.dependency 'ASIHTTPRequest', '~> 1.8.2'
end
